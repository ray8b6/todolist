import express from "express";
import mysql2 from "mysql2";
import cors from "cors";
import dotenv from "dotenv";

dotenv.config();

const port = 8000;

const app = express();

//Database connection
const database = mysql2.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
})

//If there is an authentification problem, execute the query below :
//ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Awes0299'

app.use(express.json())

app.use(cors())

//Selecting all tasks
app.get("/tasks", (request, result) => {
    const query = "SELECT * FROM Tasks"
    database.query(query, (error, data) => {
        if (error) return result.json(error)
        return result.json(data)
    })
})

//Selecting task(s) by name
app.get("/tasks/:name", (request, result) => {
    const name = '%' + request.params.name + '%';
    const query = "SELECT * FROM Tasks WHERE name like ? "

    database.query(query, [name], (error, data) => {
        if (error) return result.json(error)
        return result.json(data)
    })
})

//Insert new task
app.post("/tasks", (request, result) => {
    const values = [request.body.taskName, request.body.state]
    const query = "INSERT INTO Tasks (`name`, `state`) VALUES (?)"

    database.query(query, [values], (error, data) => {
        if (error) return result.json(error)
        return result.json("Task added successfully")

    })
})

//Update task
app.put("/tasks/:id", (request, result) => {
    const values = [request.body.taskName, request.body.taskState]
    const id = request.params.id
    const query = "UPDATE Tasks SET `name` = ?, `state` = ? WHERE idTasks = ? "

    database.query(query, [...values, id], (error, data) => {
        if (error) return result.json(error)
        return result.json("Task n°${id} updated successfully")

    })
})

//Delete task
app.delete("/tasks/:id", (request, result) => {
    const id = request.params.id
    const query = "DELETE FROM Tasks WHERE idTasks = ? "

    database.query(query, [id], (error, data) => {
        if (error) return result.json(error)
        return result.json("Task n°${id} deleted successfully")

    })
})

//Backend connection verification
app.listen(port, () => {
    console.log(`Connected to backend. Listening on port $port`)
})