# todolist

Pour pouvoir lancer l'application, il faut avoir effectué au prélable les étapes suivantes :

- Avoir créé une base de données SQL et un fichier .env à la racine du dossier **backend** dans lequel vous déclarez les variables d'environnement DB_HOST, DB_USER, DB_PASSWORD, DB_NAME pour la connexion à la base de données selon les valeurs à prendre en compte dans le fichier /bakcend/index.js

Exemple : ```DB_HOST = localhost```


- Avoir exécuté la commande `npm install` à la racine du dossier **backend** et du dossier **frontend**
