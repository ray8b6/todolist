import { Grid } from "@mui/material";
import React from "react";
import SearchBar from "../components/SearchBar";
import { CardActions, Card, CardContent } from "@mui/material";
import AddTask from "../components/AddTask";
import TaskTable from "../components/TaskTable";
import { useState, useEffect } from "react";
import axios from "axios";

const Home = () => {
  const [task, setTask] = useState([]);

  const fecthAllTasks = async () => {
    try {
      const result = await axios.get("http://localhost:8000/tasks");
      setTask(result.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fecthAllTasks();
  }, []);

  return (
    <div style={{ width: "60%", margin: "auto" }}>
      <br />

      <Grid
        container
        direction="column"
        justifyContent="flex-start"
        alignItems="strech"
      >
        <Grid item xs>
          <h1 style={{ textAlign: "center" }}>TO-DO LIST</h1>
          <br />
        </Grid>
        <Grid item xs>
          <SearchBar setTask={setTask}></SearchBar>
          <br />
        </Grid>
        <Grid item xs>
          <Card>
            <CardContent style={{ marginLeft: "20px", marginRight: "20px" }}>
              <TaskTable task={task} fecthAllTasks={fecthAllTasks}></TaskTable>
            </CardContent>
            <CardActions>
              <AddTask fetchAllTasks={fecthAllTasks}></AddTask>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
};

export default Home;
