import React from 'react';
import TextField from '@mui/material/TextField';
import { Box } from '@mui/material';
import axios from 'axios'

const SearchBar = ({ setTask }) => {

    const fecthTasks = async (event) => {
        try {
            const result = await axios.get("http://localhost:8000/tasks/" + event.target.value);
            setTask(result.data);
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <Box bgcolor="white" borderRadius={1}>
            <div style={{ width: "95%", margin: "auto", paddingBottom: "10px", paddingTop: "10px" }}>
                <TextField fullWidth
                    label="Recherche" variant="standard" onChange={fecthTasks} />
            </div>

        </Box >
    );
};

export default SearchBar;