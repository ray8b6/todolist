import React, { useState } from 'react';
import axios from 'axios';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Box } from '@mui/material';
import { FormControl, Select, MenuItem, Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@mui/material';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import CloseSharpIcon from '@mui/icons-material/CloseSharp';


export default function TaskTable({ task, fecthAllTasks }) {

    const [openedDialog, setOpenedDialog] = useState(false);

    const [selectedTask, setSelectedTask] = useState({
        idtask: 0,
        taskName: '',
        taskState: ''
    });

    const handleClickUpdate = (value) => {
        setSelectedTask({ idtask: value.idTasks, taskName: value.name, taskState: value.state })
        setOpenedDialog(true)
    };

    const handleChange = (id, name, state) => {
        setSelectedTask({ idtask: id, taskName: name, taskState: state })
    };

    const handleClickValidate = async () => {
        try {
            await axios.put("http://localhost:8000/tasks/" + selectedTask.idtask, selectedTask)
            fecthAllTasks()
        }
        catch (error) {
            console.log(error)
        }
        handleClose()
    };

    const handleClose = () => {
        setOpenedDialog(false)
    };

    const handleClickDelete = async (id) => {
        try {
            await axios.delete("http://localhost:8000/tasks/" + id)
            fecthAllTasks()
        } catch (error) {
            console.log(error)
        }
    };

    return (
        <TableContainer component={Paper} >
            <Table>
                <TableHead>
                    <TableRow >
                        <TableCell width="50%"><b>Tâche</b></TableCell>
                        <TableCell><b>Etat</b></TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {task.map(task => (
                        <TableRow
                            key={task.idTasks}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell component="th" scope="row" align='left' width="50%">
                                {task.name}
                            </TableCell>
                            <TableCell minwidth="2" align='left' >

                                <Box component="div" sx={{
                                    p: 1,
                                    borderRadius: 1,
                                    width: 90,
                                    textAlign: "center",
                                    bgcolor: task.state === 'A faire' ? '#00CED1' : task.state === 'En cours' ? 'orange' : task.state === 'Fait' ? '#3CB371' : 'rgb(234,33,23)'
                                }} >
                                    {task.state}
                                </Box>
                            </TableCell>
                            <TableCell align='right' >
                                <Button onClick={() => handleClickUpdate(task)}>
                                    <EditOutlinedIcon color='action'></EditOutlinedIcon>
                                </Button>
                                <Button onClick={() => handleClickDelete(task.idTasks)}>
                                    <CloseSharpIcon color='action'></CloseSharpIcon>
                                </Button>


                                <Dialog open={openedDialog} onClose={handleClose} >
                                    <DialogTitle>Modifier une tâche</DialogTitle>
                                    <DialogContent>
                                        <FormControl fullWidth>
                                            <br />
                                            <TextField variant='outlined' label='Nom' value={selectedTask.taskName} onChange={e => {
                                                handleChange(selectedTask.idtask, e.target.value, selectedTask.taskState)
                                            }}> </TextField>

                                            <br />
                                            <Select
                                                value={selectedTask.taskState}
                                                name='state'
                                                label="Etat"
                                                onChange={e => {
                                                    handleChange(selectedTask.idtask, selectedTask.taskName, e.target.value)
                                                }}
                                            >
                                                <MenuItem value='A faire'>A faire</MenuItem>
                                                <MenuItem value='En cours'>En cours</MenuItem>
                                                <MenuItem value='Fait'>Fait</MenuItem>
                                                <MenuItem value='En retard'>En retard</MenuItem>
                                            </Select>


                                        </FormControl>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={handleClickValidate} >Valider</Button> <Button onClick={handleClose}>Annuler</Button>
                                    </DialogActions>
                                </Dialog >

                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}