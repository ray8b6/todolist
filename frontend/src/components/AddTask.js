import { FormControl, Select, MenuItem, Box, TextField, Button } from '@mui/material';
import { Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import axios from 'axios';
import React, { useState } from 'react';

const AddTask = ({ fetchAllTasks }) => {

    const [opened, setOpened] = useState(false)

    const [task, setTask] = useState({
        taskName: '',
        state: 'A faire'
    })


    const handleClose = () => {
        setOpened(false)
    }

    const handleClickAdd = () => {
        setOpened(true)
    }

    const handleChange = (event) => {
        setTask((prevent) => ({
            ...prevent, [event.target.name]: event.target.value
        }))
    }

    const handleClickValidate = async () => {
        try {
            await axios.post("http://localhost:8000/tasks", task)
            fetchAllTasks()
        }
        catch (error) {
            console.log(error)
        }
        handleClose()
    }

    return (

        <Box sx={{ minwidth: 300 }}>
            <Button color='primary' onClick={handleClickAdd}><AddIcon color="action"></AddIcon> Ajouer une tâche</Button>
            <Dialog open={opened} onClose={handleClose} >
                <DialogTitle>Ajouter une tâche</DialogTitle>
                <DialogContent>
                    <FormControl fullWidth>
                        <br />
                        <TextField variant='outlined' label='Nom' name='taskName' onChange={handleChange}></TextField>
                        <br />
                        <Select
                            value={task.state}
                            name='state'
                            label='Etat'
                            onChange={handleChange}
                        >
                            <MenuItem value='A faire'>A faire</MenuItem>
                            <MenuItem value='En cours'>En cours</MenuItem>
                            <MenuItem value='Fait'>Fait</MenuItem>
                            <MenuItem value='En retard'>En retard</MenuItem>
                        </Select>


                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClickValidate}>Valider</Button> <Button onClick={handleClose}>Annuler</Button>
                </DialogActions>
            </Dialog>
        </Box>
    );
};

export default AddTask;